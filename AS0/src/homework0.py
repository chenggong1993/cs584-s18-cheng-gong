import sys
import numbers

import numpy as np
from numpy.linalg import inv
a=np.array([1,2,3])
b=np.array([4,5,6])
c=np.array([-1,1,3])

dot=np.dot(a,b)
dot3=np.dot(a,c)
dot4=np.dot(b,c)
print(dot)
dot2=np.dot(b,a)
print(dot2)
a2=a*a
a3=a2.sum()
b2=b*b
b3=b2.sum()
c2=c*c
c3=c2.sum()
a1=np.sqrt(a3)
b1=np.sqrt(b3)
c1=np.sqrt(c3)

cos_angle = dot / a1 / b1
angle = np.arccos(cos_angle)
angle2=angle * 360 / 2 / np.pi
print(angle2)
cos_angleac = dot3 / a1 / c1
angle = np.arccos(cos_angleac)
angle2=angle * 360 / 2 / np.pi
print(angle2)
cos_anglebc = dot4 / b1 / c1
angle = np.arccos(cos_anglebc)
angle2=angle * 360 / 2 / np.pi
print(angle2)
print(a*b)
print(b*a)
c=np.array([(1,2,3),(4,-2,3),(0,5,-1)])
d=np.array([(1,2,1),(2,1,-4),(3,-2,1)])
e=np.array([(1,2,3),(4,5,6),(-1,1,3)])
print(2*c-d)
print (np.dot(c,d))
print(np.dot(d,c))
invc=inv(np.dot(c,d))
print(invc)
invc1=inv(c)
invd=inv(d)
print(np.dot(invd,invc))
print(c.transpose())
print(d.transpose())
print(np.linalg.det(c))
print(np.linalg.det(d))
f=np.array([(1,2),(3,2)])
g=np.array([(2,-2),(-2,5)])
print(np.linalg.eig(f))
k=np.linalg.eig(f)
l=np.linalg.eig(g)
print(np.dot(np.dot(inv(k[1]),f),k[1]))
print(np.dot(k[1],k[1]))
print(np.dot(l[1],l[1]))