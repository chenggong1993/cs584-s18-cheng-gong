import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    data = np.loadtxt('svar-set1.dat')
    plt.plot(data[:, 0], data[:, 1], 'bo')

    X = data[:, 0]
    Y = data[:, 1]
    #download_dir = "1.csv"

    #csv = open(download_dir, "w")
    sumA=0
    sumB=0
    count=0
    maxa=0
    mina=0
    part=0.4
    for i in range(int(len(X)*part)):
        a=float(X[i])
        b=float(Y[i])
        #row=str(X[i])+","+str(Y[i])+"\n"
        #csv.write(row)
        sumA=a+sumA
        sumB = b + sumB
        count+=1
        if a >maxa:
            maxa=a
        if a<mina:
            mina =a
    averageA= sumA / count
    averageB= sumB / count
    sum1=0
    sum2=0
    for i in range(int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        sum1= (a-averageA)*(b-averageB)+ sum1
        sum2= (a-averageA) * (a-averageA)+ sum2
    linearb=sum1/sum2
    lineara=averageB-averageA*linearb
    print(lineara)
    print(linearb)
    x=[]
    for i in range(int(maxa-mina)):
        x.append(i)
    y=[]
    for i in range(len(x)):
        y.append(linearb*x[i]+lineara)

    trainingerror=0
    for i in range(int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        trainingerror =(linearb*a+lineara-b)*(linearb*a+lineara-b)+trainingerror
    print(trainingerror)
    testerror=0
    for i in range(len(X)-int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        testerror = (linearb * a + lineara - b) * (linearb * a + lineara - b) + testerror
    print(testerror)
    #######################################################################################
    p=np.polyfit(X,Y,1)
    trainingerror = 0
    for i in range(int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        trainingerror = (p[0] * a + p[1] - b) * (p[0] * a + p[1] - b) + trainingerror
    print(trainingerror)
    testerror = 0
    for i in range(len(X) - int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        testerror = (p[0] * a + p[1] - b) * (p[0] * a + p[1] - b) + testerror
    print(testerror)
    print(p)
    plt.plot(x,y,':r')

    plt.show()