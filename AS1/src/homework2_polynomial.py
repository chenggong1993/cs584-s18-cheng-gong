import numpy as np
from matplotlib import pyplot as plt
from numpy.linalg import inv
if __name__ == '__main__':
    data = np.loadtxt('svar-set1.dat')
    plt.plot(data[:, 0], data[:, 1], 'bo')

    X = data[:, 0]
    Y = data[:, 1]
    part=0.4
    x1=x2=x3=x4=y1=y2=y3=0
    maxa = mina = 0
    for i in range(int(len(X)*part)):
        a = float(X[i])
        b = float(Y[i])
        x1= a+x1
        x2= a*a+x2
        x3= a*a*a+x3
        x4=a*a*a*a+x4
        y1=b+y1
        y2=a*b+y2
        y3=a*a*b+y3
        if a >maxa:
            maxa=a
        if a<mina:
            mina =a
    k=int(len(X)*part)
    matrix1=np.array([[k,x1,x2],[x1,x2,x3],[x2,x3,x4]])
    matrix2=np.array([y1,y2,y3])
    result=np.dot(matrix2,inv(matrix1))
    a1=result[0]
    a2=result[1]
    a3=result[2]
    x=[]
    for i in range(int(maxa-mina)):
        x.append(i)

    y=[]
    for i in range(len(x)):
        y.append(a3*x[i]*x[i]+a2*x[i]+a1)

    trainingerror = 0
    for i in range(int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        trainingerror = (a3*a*a+a2*a+a1 - b) * (a3*a*a+a2*a+a1 - b) + trainingerror
    print(trainingerror)
    testerror = 0
    for i in range(len(X) - int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        testerror = (a3*a*a+a2*a+a1 - b) * (a3*a*a+a2*a+a1 - b) + testerror
    print(testerror)
    print(result)

    plt.plot(x, y, ':r')
    plt.show()
    p = np.polyfit(X, Y, 2)
    print(p)
    a1 = p[0]
    a2 = p[1]
    a3 = p[2]
    x = []
    for i in range(int(maxa - mina)):
        x.append(i)

    y = []
    for i in range(len(x)):
        y.append(a3 * x[i] * x[i] + a2 * x[i] + a1)

    trainingerror = 0
    for i in range(int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        trainingerror = (a3 * a * a + a2 * a + a1 - b) * (a3 * a * a + a2 * a + a1 - b) + trainingerror
    print(trainingerror)
    testerror = 0
    for i in range(len(X) - int(len(X) * part)):
        a = float(X[i])
        b = float(Y[i])
        testerror = (a3 * a * a + a2 * a + a1 - b) * (a3 * a * a + a2 * a + a1 - b) + testerror
    print(testerror)
