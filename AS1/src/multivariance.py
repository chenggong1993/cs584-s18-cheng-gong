import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt
from numpy.linalg import inv
import pandas as pd


#first make some fake data with same layout as yours

if __name__ == '__main__':
    import timeit

    start = timeit.default_timer()

    # Your statements here


    data = np.loadtxt('mvar-set3.dat')
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    zdata = data[:,0]
    xdata = data[:,1]
    ydata = data[:,2]
    ax.scatter3D(zdata, xdata, ydata, c=ydata, cmap='Greens');

    part=0.8
    c=np.array([1,zdata[0],xdata[0]])
    y=np.array(ydata[0])
    minc=maxc=zdata[0]
    minx=maxx=xdata[0]
    for i in range (1,int(len(zdata)*part)):
        if minc>zdata[i]:
            minc=zdata[i]
        if maxc<zdata[i]:
            maxc=zdata[i]
        if minx>xdata[i]:
            minx=xdata[i]
        if maxx<xdata[i]:
            maxx=xdata[i]
        x=np.array([1,zdata[i],xdata[i]])
        c=np.vstack([c,x])
        k=np.array(ydata[i])
        y=np.vstack([y,k])
    x=np.transpose(c)
    j=np.dot(x,c)

    t=np.dot(inv(j), x)
    q=np.dot(t,y)

    u = np.linspace(minc, maxc, 100)
    io = np.linspace(minx, maxx, 100)
    o=q[0]+q[1]*u+q[2]*io
    trainingerror = 0
    for i in range(int(len(zdata) * part)):
        trainingerror = q[0]+zdata[i]*q[1]+xdata[i]*q[2] + trainingerror
    meantrain=trainingerror/int(len(zdata) * part)
    print(meantrain)
    testerror = 0
    for i in range(len(zdata)-int(len(zdata) * part)):
        testerror = q[0]+zdata[i]*q[1]+xdata[i]*q[2] + testerror
    meantest=testerror/(len(zdata)-int(len(zdata) * part))
    print(testerror)
    ax.scatter3D(u, io, o,  cmap='green');
    stop = timeit.default_timer()

    print stop - start
    plt.show()
    print(q)