import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    ##############load data
    data = open('data_banknote_authentication.txt')
    data1=[]
    for line in data:
        items=line.split(",")
        items[4] = items[4].rstrip()
        for i in range (len(items)):
            items[i]=float(items[i])

        data1.append(items)

    data=np.asarray(data1)

    sum1 = count1=0
    sum2 = count2=0
    u=[]
    s=[]
    ##############initiate the parameters
    for i in range(len(data[0])*3):

        u.append(0.0000000000001)
    ##############put the 1 in the first column
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())

    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    ##########build the nonlinear matrix
    total = np.zeros(shape=(len(data), (len(data[0])-1) * 3 ))
    for i in range(len(data)):
        squre = []
        for k in range(len(data[i])-1):
            for m in range(k+1):
                squre.append(data[i][k] * data[i][m])
        squre = np.asarray(squre)
        total[i] = squre
    ###########test the parameters
    while True:

        u=np.asarray(u)
        k=np.asarray(np.matrix(u).transpose())
        count=0
        for i in range(len(total)):
            x=np.dot(np.asarray(total[i]),k)

            exp1=np.exp(-x)
            result=1/(exp1+1)
            if result>0.5 and int(data[i][5])==1:
                count+=1
            elif result<0.5 and int(data[i][5])==0:
                count+=1
        if count1 < count:
            count1 = count  ####count the successful rate
        print(count1)  ###always show the biggest accurate
        ####################if not meet requirement, modify the parameters
        if count<1372:
            for i in range(len(data)):
                x = np.dot(np.asarray(total[i]), k)
                exp1 = np.exp(-x)
                result = 1 / (exp1 + 1)
                sum1=(result-data[i][5])*0.00000001*np.asarray(total[i])+sum1
            u=u-sum1

        else:
            break
    print(u)
    print(count)
    #######1267