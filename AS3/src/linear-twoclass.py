import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    ##############load data
    data = np.loadtxt('madelon_train.data')
    data1 = np.loadtxt('madelon_train.labels')
    sum1 = count1=0
    sum2 = count2=0
    data3=[]
    data4=[]
    u=[]
    s=[]
    print(len(data1))
    ##############initiate the parameters
    for i in range(len(data[0])+1):

        u.append(0.000001)
##############put the 1 in the first column
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())

    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    #######################
    ###########test the parameters
    while True:
        sum1 = count1 = 0
        sum2 = count2 = 0
        u=np.asarray(u)
        k=np.asarray(np.matrix(u).transpose())
        count=0
        for i in range(len(data)):
            x=np.dot(np.asarray(data[i]),k)

            exp1=np.exp(x)
            exp2=np.exp(-x)
            result=(exp1-exp2)/(exp1+exp2)
            if result>0 and result<data1[i]:
                count+=1
            elif result<0 and result>data1[i]:
                count+=1
        if count1<count:
            count1=count####count the successful rate
        print(count1)###always show the biggest accurate
        ####################if not meet requirement, modify the parameters
        if count<1440:######I observed count1 that 1440 is the highest accurate I can get by initiate setting this as 415
            for i in range(len(data)):
                x = np.dot(np.asarray(data[i]), k)
                exp1 = np.exp(x)
                exp2 = np.exp(-x)
                result = (exp1 - exp2) / (exp1 + exp2)
                sum1=(result-data1[i])*0.00000000000025*np.asarray(data[i])+sum1
            u=u-sum1

        else:
            break
    ##############load test data
    data = np.loadtxt('madelon_valid.data')
    data1 = np.loadtxt('madelon_valid.labels')
    s=[]
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())
    ##############put the 1 in the first column
    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    k = np.asarray(np.matrix(u).transpose())
    count = 0
    #########test the parameters
    for i in range(len(data)):
        x = np.dot(np.asarray(data[i]), k)

        exp1 = np.exp(x)
        exp2 = np.exp(-x)
        result = (exp1 - exp2) / (exp1 + exp2)
        if result > 0 and result < data1[i]:
            count += 1
        elif result < 0 and result > data1[i]:
            count += 1
    print(count)
    #show the accurate
    ###################
    ###1461