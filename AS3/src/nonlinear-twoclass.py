import numpy as np
import sys
from matplotlib import pyplot as plt

if __name__ == '__main__':
    ##############load data
    data = np.loadtxt('madelon_train.data')
    data1 = np.loadtxt('madelon_train.labels')
    sum1 = count1=0
    sum2 = count2=0
    data3=[]
    data4=[]
    u=[]
    s=[]
    ##############initiate the parameters
    for i in range(3*(len(data[0]))+2):

        u.append(0.000000000001)
    ##############put the 1 in the first column
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())

    data = np.asarray(data)
    data = np.append(s, data, axis=1)
##########build the nonlinear matrix
    total=np.zeros(shape=(len(data),len(data[0])*3-1))
    for i in range(len(data)):
        squre = []
        for k in range(len(data[i])):
            squre.append(data[i][k])
            squre.append(int(data[i][k]*data[i][k]))
            if(k<len(data[i])-1):
                squre.append(data[i][k]*data[i][k+1])

        squre = np.asarray(squre)
        total[i]=squre
    data=total
    ###########test the parameters
    while True:
        sum1 = count1 = 0
        sum2 = count2 = 0
        u=np.asarray(u)
        k=np.asarray(np.matrix(u).transpose())
        count=0
        for i in range(len(data)):
            x=np.dot(np.asarray(data[i]),k)

            exp1=np.exp(x)
            exp2=np.exp(-x)
            result=(exp1-exp2)/(exp1+exp2)
            if result>0 and result<data1[i]:
                count+=1
            elif result<0 and result>data1[i]:
                count+=1
        if count1<count:
            count1=count####count the successful rate
        print(count1)###always show the biggest accurate
        ####################if not meet requirement, modify the parameters
        print(count1)
        if count<1709:######I observed count1 that 1709 is the highest accurate I can get by initiate setting this as 415
            for i in range(len(data)):
                x = np.dot(np.asarray(data[i]), k)
                exp1 = np.exp(x)
                exp2 = np.exp(-x)
                result = (exp1 - exp2) / (exp1 + exp2)
                sum1=(result-data1[i])*0.0000000000000000025*np.asarray(data[i])+sum1
            u=u-sum1

        else:
            break
    ##############load test data
    data = np.loadtxt('madelon_valid.data')
    data1 = np.loadtxt('madelon_valid.labels')
    s=[]
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())
    ##############put the 1 in the first column
    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    k = np.asarray(np.matrix(u).transpose())
    ##########build the nonlinear matrix
    total = np.zeros(shape=(len(data), len(data[0]) * 3 - 1))
    for i in range(len(data)):
        squre = []
        for k in range(len(data[i])):
            squre.append(data[i][k])
            squre.append(int(data[i][k] * data[i][k]))
            if (k < len(data[i]) - 1):
                squre.append(data[i][k] * data[i][k + 1])

        squre = np.asarray(squre)
        total[i] = squre
    data = total
    u = np.asarray(u)
    k = np.asarray(np.matrix(u).transpose())
    count = 0
    #########test the parameters
    for i in range(len(data)):
        x = np.dot(np.asarray(data[i]), k)

        exp1 = np.exp(x)
        exp2 = np.exp(-x)
        result = (exp1 - exp2) / (exp1 + exp2)
        if result > 0 and result < data1[i]:
            count += 1
        elif result < 0 and result > data1[i]:
            count += 1
    print(count)
###########1709
