import numpy as np
from matplotlib import pyplot as plt

if __name__ == '__main__':
    ##############load data
    data = open('monks-1.train')
    data1=[]
    for line in data:
        items=line.split(" ")
        k=[]
        for i in range (1,len(items)-1):
            k.append(int(items[i]))
        data1.append(k)
    data=np.asarray(data1)
    sum1 = count1=0
    sum2 = count2=0
    u=[]
    s=[]
    ##############initiate the parameters
    for i in range(len(data[0])+1):

        u.append(0)
    ##############put the 1 in the first column
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())

    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    print(len(data))
    #######################################################
    ##############load data
    data2 = open('monks-2.train')
    data1 = []
    for line in data2:
        items=line.split(" ")
        k=[]
        for i in range (1,len(items)-1):
            k.append(int(items[i]))
        data1.append(k)
    data2 = np.asarray(data1)
    sum1 = count1 = 0
    sum2 = count2 = 0
    u2 = []
    s2 = []
    ##############initiate the parameters
    for i in range(len(data2[0])+1):
        u2.append(0)
    ##############put the 1 in the first column
    for i in range(len(data2)):
        s2.append(1)
    s2 = np.asarray(np.matrix(s2).transpose())

    data2 = np.asarray(data2)
    data2 = np.append(s2, data2, axis=1)
    print(len(data2))
    ###################################################################
    ##############load data
    data3 = open('monks-3.train')
    data1 = []
    for line in data3:
        items=line.split(" ")
        k=[]
        for i in range (1,len(items)-1):
            k.append(int(items[i]))
        data1.append(k)
    data3 = np.asarray(data1)
    sum1 = count1 = 0
    sum2 = count2 = 0
    sum3=0
    u3 = []
    s3 = []
    ##############initiate the parameters
    for i in range(len(data3[0])+1):
        u3.append(0)
    ##############put the 1 in the first column
    for i in range(len(data3)):
        s3.append(1)
    s3 = np.asarray(np.matrix(s3).transpose())

    data3 = np.asarray(data3)
    data3 = np.append(s3, data3, axis=1)
    print(len(data3))
    ########################################################
    #########test the parameters
    while True:

        u=np.asarray(u)
        k=np.asarray(np.matrix(u).transpose())
        u2 = np.asarray(u2)
        k2 = np.asarray(np.matrix(u2).transpose())
        u3 = np.asarray(u3)
        k3 = np.asarray(np.matrix(u3).transpose())
        count=0
        ###test for class1
        ran = 0.8
        for i in range((int(len(data)*ran))):
            x=np.dot(np.asarray(data[i]),k)
            x2 = np.dot(np.asarray(data[i]), k2)
            x3 = np.dot(np.asarray(data[i]), k3)
            exp1=np.exp(x)
            exp2 = np.exp(x2)
            exp3 = np.exp(x3)
            result=exp1/(exp1+exp2+exp3)
            result=result*2
            if result>0.3333333333334 :
                count+=1 #collect all the corrrect data
        ###test for class2
        for i in range((int(len(data)*ran)),len(data)):
            x=np.dot(np.asarray(data[i]),k)
            x2 = np.dot(np.asarray(data[i]), k2)
            x3 = np.dot(np.asarray(data[i]), k3)
            exp1=np.exp(x)
            exp2 = np.exp(x2)
            exp3 = np.exp(x3)
            result=exp1/(exp1+exp2+exp3)
            if result>0.3333333333334 :
                count+=1 #collect all the corrrect data
        ###test for class2

        for i in range((int(len(data2)))):
            x=np.dot(np.asarray(data2[i]),k)
            x2 = np.dot(np.asarray(data2[i]), k2)
            x3 = np.dot(np.asarray(data2[i]), k3)
            exp1=np.exp(x)
            exp2 = np.exp(x2)
            exp3 = np.exp(x3)
            result=exp2/(exp1+exp2+exp3)
            if result>0.3333333333334:
                count+=1#collect all the corrrect data
        ##test for class3
        for i in range((int(len(data3)))):
            x=np.dot(np.asarray(data3[i]),k)
            x2 = np.dot(np.asarray(data3[i]), k2)
            x3 = np.dot(np.asarray(data3[i]), k3)
            exp1=np.exp(x)
            exp2 = np.exp(x2)
            exp3 = np.exp(x3)
            result=exp3/(exp1+exp2+exp3)
            if result>0.3333333333334 :
                count+=1#collect all the corrrect data
        if count1 < count:
            count1 = count  ####count the successful rate
        print(count1)  ###always show the biggest accurate
        ####################if not meet requirement, modify the parameters
        if count<410:######I observed count1 that 262 is the highest accurate I can get by initiate setting this as 415
            ############modify parameters for class1
            for i in range(len(data)):
                x = np.dot(np.asarray(data[i]), k)
                x2 = np.dot(np.asarray(data[i]), k2)
                x3 = np.dot(np.asarray(data[i]), k3)
                exp1 = np.exp(x)
                exp2 = np.exp(x2)
                exp3 = np.exp(x3)
                result = exp1 / (exp1 + exp2 + exp3)
                sum1=(result-1)*0.00000001*np.asarray(data[i])+sum1
            u=u-sum1
            ############modify parameters for class2
            for i in range(len(data2)):
                x = np.dot(np.asarray(data2[i]), k)
                x2 = np.dot(np.asarray(data2[i]), k2)
                x3 = np.dot(np.asarray(data2[i]), k3)
                exp1 = np.exp(x)
                exp2 = np.exp(x2)
                exp3 = np.exp(x3)
                result = exp2 / (exp1 + exp2 + exp3)
                sum2=(result-2)*0.00000001*np.asarray(data2[i])+sum2
            u2=u2-sum2
            ############modify parameters for class3
            for i in range(len(data3)):
                x = np.dot(np.asarray(data3[i]), k)
                x2 = np.dot(np.asarray(data3[i]), k2)
                x3 = np.dot(np.asarray(data3[i]), k3)
                exp1 = np.exp(x)
                exp2 = np.exp(x2)
                exp3 = np.exp(x3)
                result = exp3 / (exp1 + exp2 + exp3)
                sum3=(result-3)*0.000000001*np.asarray(data3[i])+sum3
            u3=u3-sum3

        else:
            break
    print(u,u3,u2)
    print(count)
    #####################################################
    ##############load test data
    data = open('monks-1.test')
    data1 = []
    for line in data:
        items = line.split(" ")
        k = []
        for i in range(1, len(items) - 1):
            k.append(int(items[i]))
        data1.append(k)
    data = np.asarray(data1)
    sum1 = count1 = 0
    sum2 = count2 = 0
    u = []
    s = []
    ##############initiate the parameters
    for i in range(len(data[0]) + 1):
        u.append(0)
    ##############put the 1 in the first column
    for i in range(len(data)):
        s.append(1)
    s = np.asarray(np.matrix(s).transpose())

    data = np.asarray(data)
    data = np.append(s, data, axis=1)
    print(len(data))
    #######################################################
    ##############load test data
    data2 = open('monks-2.test')
    data1 = []
    for line in data2:
        items = line.split(" ")
        k = []
        for i in range(1, len(items) - 1):
            k.append(int(items[i]))
        data1.append(k)
    data2 = np.asarray(data1)
    sum1 = count1 = 0
    sum2 = count2 = 0
    u2 = []
    s2 = []
    ##############initiate the parameters
    for i in range(len(data2[0]) + 1):
        u2.append(0)

    for i in range(len(data2)):
        s2.append(1)
    s2 = np.asarray(np.matrix(s2).transpose())
    ##############put the 1 in the first column
    data2 = np.asarray(data2)
    data2 = np.append(s2, data2, axis=1)
    print(len(data2))
    ###################################################################
    ##############load test data
    data3 = open('monks-3.test')
    data1 = []
    for line in data3:
        items = line.split(" ")
        k = []
        for i in range(1, len(items) - 1):
            k.append(int(items[i]))
        data1.append(k)
    data3 = np.asarray(data1)
    sum1 = count1 = 0
    sum2 = count2 = 0
    sum3 = 0
    u3 = []
    s3 = []
    ##############initiate the parameters
    for i in range(len(data3[0]) + 1):
        u3.append(0)
    ##############put the 1 in the first column

    for i in range(len(data3)):
        s3.append(1)
    s3 = np.asarray(np.matrix(s3).transpose())

    data3 = np.asarray(data3)
    data3 = np.append(s3, data3, axis=1)
    print(len(data3))
    u = np.asarray(u)
    k = np.asarray(np.matrix(u).transpose())
    u2 = np.asarray(u2)
    k2 = np.asarray(np.matrix(u2).transpose())
    u3 = np.asarray(u3)
    k3 = np.asarray(np.matrix(u3).transpose())
    count = 0
    #########test the parameters
    ###for class1
    for i in range(len(data)):
        x = np.dot(np.asarray(data[i]), k)
        x2 = np.dot(np.asarray(data[i]), k2)
        x3 = np.dot(np.asarray(data[i]), k3)
        exp1 = np.exp(x)
        exp2 = np.exp(x2)
        exp3 = np.exp(x3)
        result = exp1 / (exp1 + exp2 + exp3)

        if result > 0.3333333333333:
            count += 1
    ###for class2
    for i in range(len(data2)):
        x = np.dot(np.asarray(data2[i]), k)
        x2 = np.dot(np.asarray(data2[i]), k2)
        x3 = np.dot(np.asarray(data2[i]), k3)
        exp1 = np.exp(x)
        exp2 = np.exp(x2)
        exp3 = np.exp(x3)
        result = exp2 / (exp1 + exp2 + exp3)
        if result > 0.3333333333333:
            count += 1
    ###for class3
    for i in range(len(data3)):
        x = np.dot(np.asarray(data3[i]), k)
        x2 = np.dot(np.asarray(data3[i]), k2)
        x3 = np.dot(np.asarray(data3[i]), k3)
        exp1 = np.exp(x)
        exp2 = np.exp(x2)
        exp3 = np.exp(x3)
        result = exp3 / (exp1 + exp2 + exp3)
        if result > 0.3333333333333:
            count += 1
    print(count)###########test result
    ###############1257