import numpy as np
from matplotlib import pyplot as plt
import random
from cvxopt import matrix, solvers
def linear_gram(x):
    s = np.zeros((len(x), len(x)))
    a = 0.5
    b = 0.5
    for i in range(len(x)):
        for k in range(len(x)):
            s[i][k] = 0.5 * np.dot(x[i], np.transpose(x[k])) + 0.5
    return s  ##########linear kernel


def poly_gram(x):
    s = np.zeros((len(x), len(x)))

    for i in range(len(x)):
        for k in range(len(x)):
            s[i][k] = (np.dot(x[i], np.transpose(x[k])) + 1) ** 0.5

    return s  ###########polynomial kernel


def guassian_gram(x):
    s = np.zeros((len(x), len(x)))

    for i in range(len(x)):
        for k in range(len(x)):
            s[i][k] = np.exp(np.dot(x[i] - x[k], np.transpose(x[i] - x[k])) / -0.1)

    return s  ############guassian kernel


if __name__ == '__main__':
    a = 0.5
    b = 0.7
    class1 = np.zeros((50, 2))
    class2 = np.zeros((50, 2))
    class3 = np.zeros((len(class1) + len(class2), 2))  #####collacte all dataset
    class5 = np.zeros((len(class1) + len(class2), 1))
    for i in range(len(class1)):
        k = random.uniform(50, 70)
        class1[i][0] = k
        class1[i][1] = a * k + b + random.uniform(1, 10)
        if len(class2) > i:
            class2[i][0] = k
            class2[i][1] = a * k + b - random.uniform(1, 10)
    ###############################put all data to class3---x and class5--y
    for i in range(len(class1)):
        class3[i][0] = class1[i][0]
        class3[i][1] = class1[i][1]
        class5[i] = 1
    for i in range(len(class1), len(class3)):
        class3[i][0] = class2[i - len(class1)][0]
        class3[i][1] = class2[i - len(class1)][1]
        class5[i] = -1
    ############################################################

    q = np.zeros((len(class1) + len(class2), 1))  ########create q
    c = 0.1  ##initiate c
    for i in range(len(class3)):
        q[i] = -1
    G = matrix(np.diag(np.ones(len(class1) + len(class2)) * -1))  #########G for solvers
    h = np.zeros((len(class1) + len(class2), 1))  #####h
    G_slack = matrix(np.diag(np.ones(len(class1) + len(class2))))  ########add slack
    h_slack = matrix(np.ones(len(class1) + len(class2)) * c)  #######add c
    G = matrix(np.vstack((G, G_slack)))
    h = matrix(np.vstack((h, h_slack)))
    A = matrix(class5.reshape(1, -1))
    b = matrix(np.zeros(1))
    maxp = linear_gram(class3)  #######xixjyiyj
    P = matrix(np.outer(class5, class5) * maxp)

    q = matrix(q)
    h = matrix(h)

    sol = solvers.qp(P, q, G, h, A, b)

    alpha = np.array(sol['x'])  ########this is the alpha
    w = np.sum(alpha * class5 * class3, axis=0)  ##calculatet the W
    cond = (alpha > 1e-5).reshape(-1)  #########get the support vector
    class109 = class3[cond]
    class100 = np.zeros((len(class109), 2))
    for i in range(len(class109)):
        class100[i] = class109[i][0] * w[0] + class109[i][1] * w[1]
    b = class5[cond] - class100  #########wo is calculated
    bias = b[0]
    slope = -w[0] / w[1]
    intercept = -bias / w[1]
    #####################plot the data
    c = np.zeros((71, 1))
    for i in range(10, 81):
        c[i - 10] = i
    plt.plot(c, c * slope + intercept, 'k-')
    for i in range(len(class3)):
        for k in range(len(class109)):
            if (class3[i][0]) == (class109[k][0]) and (class3[i][1]) == (class109[k][1]):
                np.delete(class3, i, 0)
            else:
                plt.scatter(class3[i][0], class3[i][1], color='blue')
    for k in range(len(class109)):
        plt.scatter(class109[k][0], class109[k][1], color='green')

    plt.show()