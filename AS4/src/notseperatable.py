import numpy as np
from matplotlib import pyplot as plt
import random
from cvxopt import matrix, solvers

from numpy.linalg import inv

if __name__ == '__main__':
    a = 0.5
    b = 0.7
    class1 = np.zeros((400, 2))
    class2 = np.zeros((20, 2))
    class3 = np.zeros((len(class1) + len(class2), 2))  #####collacte all dataset
    class5 = np.zeros((len(class1) + len(class2), 1))
    for i in range(len(class1)):
        k = random.uniform(50, 70)
        class1[i][0] = k
        class1[i][1] = a * k + b + random.uniform(1, 10)
        if len(class2) > i:
            class2[i][0] = k
            class2[i][1] = a * k + b - random.uniform(1, 10)
    ###############################put all data to class3---x and class5--y
    for i in range(len(class1)):
        class3[i][0] = class1[i][0]
        class3[i][1] = class1[i][1]
        class5[i] = 1
    for i in range(len(class1), len(class3)):
        class3[i][0] = class2[i - len(class1)][0]
        class3[i][1] = class2[i - len(class1)][1]
        class5[i] = -1
    ############################################################
    nons=np.zeros((50,1))#######put random label to classone
    for i in range(len(nons)):
        if i %2==0:
            nons[i]=1
        else:
            nons[i]=-1
    ######################get all parameters to use solvers
    q=np.zeros((50,1))
    for i in range(len(class1)):
        q[i]=-1
    G= matrix(-np.eye(50))##no slack
    h=np.zeros((50,1))##no c
    A = matrix(nons.reshape(1, -1))
    b = matrix(np.zeros(1))
    maxp=nons*class1
    K=np.dot(maxp,np.transpose(maxp))

    maxp3=matrix(K)
    q=matrix(q)
    h=matrix(h)

    sol=solvers.qp(maxp3,q,G,h,A,b)
#############################################################
    alpha=np.array(sol['x'])
    #alpha
    w = np.sum(alpha * nons * class1, axis=0)##########get w
    cond = (alpha > 1e-4).reshape(-1)
    class109=class1[cond]#########support vector
    class100 = np.zeros((len(class109), 2))
    for i in range(len(class109)):
        class100[i]=class109[i][0]*w[0]+class109[i][1]*w[1]
    b = nons[cond] - class100##########w0 get

    bias = b[0]

    slope = -w[0] / w[1]
    intercept = -bias / w[1]
##############plot dataset
    plt.plot(class1[0], class1[0] * slope + intercept, 'k-')
    for i in range (len(class1)):

        plt.scatter(class1[i][0],class1[i][1],color='blue')
    for i in range (len(class1)):

        plt.scatter(class2[i][0],class2[i][1],color='red')


    plt.show()
