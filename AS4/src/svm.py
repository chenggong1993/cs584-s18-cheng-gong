import numpy as np
from matplotlib import pyplot as plt
import random
from cvxopt import matrix, solvers

from numpy.linalg import inv

if __name__ == '__main__':
    a = 0.5
    b = 0.7
    class1 = np.zeros((400, 2))
    class2 = np.zeros((20, 2))
    class3 = np.zeros((len(class1) + len(class2), 2))  #####collacte all dataset
    class5 = np.zeros((len(class1) + len(class2), 1))
    for i in range(len(class1)):
        k = random.uniform(50, 70)
        class1[i][0] = k
        class1[i][1] = a * k + b + random.uniform(1, 10)
        if len(class2) > i:
            class2[i][0] = k
            class2[i][1] = a * k + b - random.uniform(1, 10)
    ###############################put all data to class3---x and class5--y
    for i in range(len(class1)):
        class3[i][0] = class1[i][0]
        class3[i][1] = class1[i][1]
        class5[i] = 1
    for i in range(len(class1), len(class3)):
        class3[i][0] = class2[i - len(class1)][0]
        class3[i][1] = class2[i - len(class1)][1]
        class5[i] = -1
    ############################################################
    ######################get all parameters to use solvers
    q=np.zeros((100,1))

    for i in range(len(class3)):
        q[i]=-1
    G= matrix(-np.eye(100))##no slack
    h=np.zeros((100,1))##no c
    A = matrix(class5.reshape(1, -1))
    b = matrix(np.zeros(1))
    maxp=class5*class3
    K=np.dot(maxp,np.transpose(maxp))
    maxp3=matrix(K)
    q=matrix(q)
    h=matrix(h)
    sol=solvers.qp(maxp3,q,G,h,A,b)
    #############################################################
    alpha=np.array(sol['x'])#alpha
    w = np.sum(alpha * class5 * class3, axis=0)##########get w

    cond = (alpha > 1e-6).reshape(-1)#########support vector

    class109=class3[cond]

    plt.show()
    class100 = np.zeros((len(class109), 2))
    for i in range(len(class109)):
        class100[i]=class109[i][0]*w[0]+class109[i][1]*w[1]
    b = class5[cond] - class100##########w0 get

    bias = b[0]

    slope = -w[0] / w[1]
    intercept = -bias / w[1]
    c=np.zeros((100,1))
    for i in range(0,100):
        c[i]=i
    plt.plot(c, c * slope + intercept, 'k-')
    ##############plot dataset
    for i in range (len(class3)):
        for k in range(len(class109)):
            if (class3[i][0])==(class109[k][0]) and (class3[i][1])==(class109[k][1]):
                np.delete(class3,i,0)
            else:
                plt.scatter(class3[i][0],class3[i][1],color='blue')
    for k in range(len(class109)):
        plt.scatter(class109[k][0], class109[k][1], color='green')


    plt.show()


