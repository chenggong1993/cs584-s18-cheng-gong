import numpy as np
from matplotlib import pyplot as plt
import random
from numpy.linalg import inv
if __name__ == '__main__':
    ##############load data
    data = open('abode1.csv')
    data1=[]
    for line in data:
        items=line.split(",")
        items[6] = items[6].rstrip()
        k=[]
        for i in range (int(len(items))):
            k.append(float(items[i]))
        data1.append(k)
    data=np.asarray(data1)
#####################################
    c=[]
    for i in range(len(data)-1):
        b = []
        for k in range(len(data)-1):
            b.append(0.0000001)
        b = np.asarray(b)
        c.append(b)
    c = np.asarray(c)
    ########################################this the b of the matix
    pr=np.zeros((len(data)-1,7))######initiate the xpb
    count=0#####for counting how many points match the expectation
    ############# because the data include the stock price,delete it
    data1 = np.delete(data, 6, 1)
    data2 = np.delete(data1, len(data1)-1, 0)#########predict stock price, last row is redundent
    counttotal=[]
    count1=0
    v=np.zeros((len(data)-1,6))##########v value initiated
    for i in range (len(v)):
        for k in range (len(v[i])):
            v[i][k]=2
    data3=data2#########x sb initiated
    data4=data2
    K=[]
    count6=count5=count14=0
    while(True):
        #########get K
        count5+=1
        K=np.dot(data2,np.transpose(data2))
        K=0.5*np.tanh((0.00000001*K+0.0001))+c###kernel function for K
        K=np.asarray(K)
        result=[]
        ############the result is sum of each row to get 1* m vector
        for i in range(len(K)):
            sum=0
            for u in range(len(K[i])):
                sum=K[i][u]+sum
            result.append(sum)
        ####################################################
        count=0
        for i in range(len(result)):

            if abs(result[i]-data[i+1][6])<=1:
                count+=1
        #########################the count is counting how many points match the requirement
        counttotal.append(count)
        print(count1)
        pltmap = []
        for i in range(len(data)):
            pltmap.append(data[i][6])
        if count<200:#########if smaller than 1200. change the X
            for i in range(len(result)):
                if abs(result[i]-data[i+1][6])<abs(pr[i][6]-data[i+1][6]):####if smaller than the stock price in PR, than the row in this pr = X
                    for index in range(len(pr[i])-1):

                        pr[i][index]=data2[i][index]
                        pr[i][6]=result[i]
            ##########################get the PB first

            count6+=1
            if count>count1:
                count6 += 1
                count14=count5
                count1=count
                data3 = data2
            if count5-count14>=2000:
                print('this run is fail, plz start new run')

            ###########get the x sb in here

            pr1=np.delete(pr,6,1)##we don't need the stock price
            v=v*0.9+0.7*random.uniform(0,1)*(pr1-data2)+1*random.uniform(0,1)*(data3-data2)########## change the X

            data2=data2+v ######## new data
            #print(data2[0])
        else:

            break
    datainv=inv(np.dot(np.transpose(data4),data4))
    datainv=np.asarray(datainv)

    datavector=np.dot(datainv,np.transpose(data4))

    datavector2 = np.dot(datavector, K)

    pltmap=[]
    result10=[]
    q=np.dot((data4),datavector2)

    sum=0
    for i in range (len(K[0])):
        sum=K[0][i]+sum
    for i in range(len(q)):
        sum = 0
        for u in range(len(q[i])):
            sum = q[i][u] + sum

        result10.append(sum)


    for i in range(len(data)):
        pltmap.append(data[i][6])

    plt.plot(pltmap)
    plt.plot(result)
    plt.show()
    ############################################################################
    datatest = open('1.csv')
    datatest1 = []
    for line in datatest:
        items = line.split(",")
        items[6] = items[6].rstrip()
        k = []
        for i in range(int(len(items))):
            k.append(float(items[i]))
        datatest1.append(k)
    datatest = np.asarray(datatest1)
    data10=data4
    test = []
    for s in range(len(data4),len(datatest)):
        for i in range(len(data10)):
            for k in range(len(data10[i])):

                data10[i][k]=datatest[i+s-len(data4)][k]#############predict everyday's future stock price
        K = np.dot(data10, datavector2)
        testsum=0
        for u in range(len(K[len(K)-1])):
            testsum=K[len(K)-1][u]+testsum
        result.append(testsum)
    pltmap=[]
    for i in range(len(datatest)):
        pltmap.append(datatest[i][6])

    plt.plot(pltmap)
    plt.plot(result)
    plt.show()